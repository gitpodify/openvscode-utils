# OpenVSCode Utils

An set of utilities for OpenVSCode Server, including an updater script and an Bash wrapper for OpenVSCode server. Also hosts an API for querying the latest version programmatically.

**WARNING**: These utilities are not yet tested, so proceed at your own risk.

## Requirements

* Bash
* curl and wget
* tar
* jq
* Git

## Installation

### One-liner, automated

* With curl: `curl -fsSL https://gitlab.com/gitpodify/openvscode-utils/raw/main/install.sh | bash -`

### Manual

1. Clone the repository into `~/.openvscode-utils` directory via `git clone https://gitlab.com/gitpodify/openvscode-utils ~/.openvscode-utils`.
2. Manually copy the following snippet to your Bash-compartible shell login/init script file.

```bash
# Defaults from this repo's scripts, especially the updater, customize as you want.
export OPENVSCODE_ROOT=$HOME/.gitpodified-vscode

#
export PATH="$HOME/.openvscode-utils/bin:$PATH"
```

3. Reload the shell by sourcing your your Bash-compartible shell login/init script file with `source ~/.<bashrc|bash_profile|zshrc>`.
4. Check if you can now run it with `code` or `gp-code`.

## Supported variables

* `OPENVSCODE_INSIDERS` - Set this to `insiders` to install the nightly/Insiders builds.
* `SKIP_VERSION_CHECKS` - Set to `false` to trigger update checking before starting the server when invoking `./bin/code` from this repo.
